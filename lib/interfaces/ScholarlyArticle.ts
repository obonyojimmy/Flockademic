import { Periodical } from './Periodical';
import { Person } from './Person';

export interface ScholarlyArticle {
  author: Array<Partial<Person>>;
  associatedMedia: Array<{
    contentUrl: string;
    // tslint:disable-next-line:max-line-length
    license?: 'https://creativecommons.org/licenses/by/4.0/' | 'https://arxiv.org/licenses/nonexclusive-distrib/1.0/license.html' | string,
    name: string;
  }>;
  description: string;
  identifier: string;
  isPartOf: Partial<Periodical> & { identifier: string };
  name: string;
  datePublished: string;
  url: string;
  sameAs?: string;
}

export type InitialisedScholarlyArticle = Partial<ScholarlyArticle> & { identifier: string; };

export type PublishedScholarlyArticle = InitialisedScholarlyArticle & { author: Person[]; datePublished: string; };
