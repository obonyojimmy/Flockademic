import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import {
  ErrorPage,
} from '../../src/components/errorPage/component';

it('should display an error page', () => {
  const mockProps = {
    children: 'Some text',
    title: 'Some title',
    url: 'some-url',
  };

  const page = shallow(<ErrorPage {...mockProps}/>);

  expect(toJson(page)).toMatchSnapshot();
});
