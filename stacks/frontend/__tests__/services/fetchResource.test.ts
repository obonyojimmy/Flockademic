import { fetchResource } from '../../src/services/fetchResource';

it('should delegate calls to native `fetch`, prepending the API URL', () => {
  process.env.API_URL = 'http://example.com';
  fetchResource('/arbitrary-resource');

  // fetch is mocked by jest-fetch-mock
  expect(fetch.mock.calls.length).toBe(1);
  expect(fetch.mock.calls[0][0]).toBe('http://example.com/arbitrary-resource');
});

it('should not remove leading slashes to enforce consistency', () => {
  process.env.API_URL = 'http://example.com';
  fetchResource('arbitrary-resource');

  // fetch is mocked by jest-fetch-mock
  expect(fetch.mock.calls.length).toBe(1);
  expect(fetch.mock.calls[0][0]).toBe('http://example.comarbitrary-resource');
});

it('should pass on options to `fetch`', () => {
  process.env.API_URL = 'http://example.com';
  fetchResource('arbitrary-resource', { method: 'arbitrary' });

  // fetch is mocked by jest-fetch-mock
  expect(fetch.mock.calls.length).toBe(1);
  expect(fetch.mock.calls[0][0]).toBe('http://example.comarbitrary-resource');
  expect(fetch.mock.calls[0][1]).toEqual({ method: 'arbitrary' });
});