import * as React from 'react';

interface Props {
  doi: string;
}

// Due to inline DOM manipulation, testing this component in a Node environment is problematic.
// Since it's a relatively small component, we simply skip testing it:
/* istanbul ignore next */
export class PlauditWidget extends React.PureComponent<Props> {
  public componentDidMount() {
    // The container has loaded now, and React should not remove it (thanks to dangerouslySetInnerHTML),
    // so now inject the script there so it can execute.
    const scriptElement = document.createElement('script');
    scriptElement.setAttribute('src', `https://plaudit.pub/embed/endorsements.js`);
    scriptElement.setAttribute('async', 'async');
    scriptElement.dataset.pid = `doi:${this.props.doi}`;
    scriptElement.dataset.embedderId = 'flockademic';
    document.getElementById('scriptContainer')!.appendChild(scriptElement);
  }

  public render() {
    const scriptContainer = {
      __html: '<div id="scriptContainer"/>',
    };

    return (
      <div dangerouslySetInnerHTML={scriptContainer}/>
    );
  }
}
