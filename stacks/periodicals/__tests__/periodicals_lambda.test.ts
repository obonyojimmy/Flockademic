jest.mock('../../../lib/lambda/faas');
jest.mock('../../../lib/lambda/middleware/withSession');
jest.mock('../../../lib/lambda/middleware/withDatabase');

import { handler } from '../src/periodicals_lambda';

describe('The periodicals lambda handler', () => {
  it('should handle all supported routes and supply a fallback', () => {
    const mockInitialise = require.requireMock('../../../lib/lambda/faas').initialise;
    const mockFaas = {
      default: jest.fn(),
      get: jest.fn(),
      post: jest.fn(),
      put: jest.fn(),
    };
    mockInitialise.mockReturnValueOnce(mockFaas);

    handler('Arbitrary event' as any, 'Arbitrary context' as any);

    expect(mockFaas.get.mock.calls).toMatchSnapshot();
    expect(mockFaas.post.mock.calls).toMatchSnapshot();
    expect(mockFaas.put.mock.calls).toMatchSnapshot();
    expect(mockFaas.default).toHaveBeenCalled();
  });

  it('should respond with an error for invalid methods', () => {
    let callback;

    const mockInitialise = require.requireMock('../../../lib/lambda/faas').initialise;
    const mockFaas = {
      default: (cb) => {
        callback = cb;
      },
      get: jest.fn(),
      post: jest.fn(),
      put: jest.fn(),
    };
    mockInitialise.mockReturnValueOnce(mockFaas);

    handler('Arbitrary event' as any, 'Arbitrary context' as any);

    return expect(callback({ method: 'GET', path: '/some_path' })).rejects
      .toEqual(new Error('Invalid method: GET /periodicals/some_path'));
  });
});
