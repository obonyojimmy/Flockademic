import { Account } from '../../../../lib/interfaces/Account';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function publishScholarlyArticle(
  database: Database,
  articleIdentifier: string,
  account: Account,
): Promise<{
  datePublished: string;
  identifier: string;
}> {
  interface PublicationRow {
    identifier: string;
    date_published: Date;
  }

  const result: [ undefined, PublicationRow ] = await database.tx((t) => {
    const queries = [
      // Make sure the current account is owner of the periodical
      t.none(
        // tslint:disable-next-line:no-invalid-template-strings
        'INSERT INTO scholarly_article_authors (scholarly_article, author) VALUES (${articleId}, ${authorId})'
          + ' ON CONFLICT (scholarly_article, author) DO NOTHING',
        { articleId: articleIdentifier, authorId: account.identifier },
      ),
      // Publish the periodical
      t.one(
        'UPDATE scholarly_articles'
        + ' SET date_published=NOW()'
          // tslint:disable-next-line:no-invalid-template-strings
        + ' WHERE identifier=${articleId}'
        + ' RETURNING identifier, date_published',
        { articleId: articleIdentifier },
      ),
    ];

    return t.batch(queries);
  });

  return {
    datePublished: result[1].date_published.toISOString(),
    identifier: result[1].identifier,
  };
}
